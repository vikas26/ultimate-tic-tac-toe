from random import randint, randrange
import minimax as mm
import math

class RandomBot:
    
    def get_move(self, pos, tleft):
        lmoves = pos.legal_moves()
        rm = randint(0, len(lmoves)-1)
        return lmoves[rm]

    def get_minimax_move(self, pos, tleft):
        available_macroboards = pos.get_available_macroboards()
        macroboard_number = available_macroboards[0]

        # macroboards_state
        macroboards_state = self.get_macroboards_state(pos)

        # all empty boards - first chance given to me
        if (len(available_macroboards) > 8):
            macroboard_number = 5

        # more than 1 boards i can play in
        if (len(available_macroboards) > 1 and len(available_macroboards) < 9):
            macroboard = [0 if x==-1 else x for x in pos.macroboard]
            macroboard_numbers = mm.init(macroboard, self.myid, self.oppid, True)
            for i in range(len(macroboard_numbers)):
                if pos.macroboard[macroboard_numbers[i][0]-1] == -1:
                    macroboard_number = macroboard_numbers[i][0]
                    break

        # get board data from macroboard number
        minimax_board = pos.get_board_data(macroboard_number)

        # empty board
        if (sum(minimax_board) == 0):
            coordinates_to_play_in = [[1, 1], [3, 1], [7, 1], [9, 1], [2, 1], [4, 1], [6, 1], [8, 1]]
        else:
            coordinates_to_play_in = mm.init(minimax_board, self.myid, self.oppid, True)

        coordinate = mm.choose_final_response (coordinates_to_play_in, macroboards_state)

        mx = ((macroboard_number - 1) % 3) *3
        my = int(math.floor(((macroboard_number - 1) / 3))) *3
        return (mx + (coordinate - 1)%3, my + int(math.floor((coordinate - 1)/3)))

    def get_macroboards_state(self, pos):
        macroboards_state = {
            'fresh' : [],
            'match_wld' : [],
            'match_won_by' : [],
            'match_draw_move_yes' : []
        }
        macroboards_board_data = []
        for i in range(9):
            macroboards_board_data.append(pos.get_board_data(i+1))

            macroboards_state['fresh'].append(False)
            macroboards_state['match_wld'].append(False)
            macroboards_state['match_won_by'].append(0)
            macroboards_state['match_draw_move_yes'].append(False)

            # match win/lose/draw
            if pos.macroboard[i] > 0:
                macroboards_state['match_won_by'][i] = pos.macroboard[i]
                macroboards_state['match_wld'][i] = True

            # fresh board
            macroboards_state['fresh'][i] = self.fresh_board (macroboards_board_data[i])
            
        return macroboards_state

    def fresh_board (self, board):
        if sum(board) == 0:
            return True
        return False
