import copy
import math
game = {}

def minimax (board, ttt_pos, myid, oppid, depth):
    final_response = []
    possible_moves = get_possible_moves (board)
    result = check_win (board)
    if (len(possible_moves) < 1 or result > 0):
        ttt_pos.append (result)
        ttt_pos.append (depth)
        return ttt_pos

    for possible_move in possible_moves:
        new_board = make_move (copy.deepcopy(board), possible_move, myid)
        new_ttt_pos = copy.deepcopy (ttt_pos)
        new_ttt_pos.append (possible_move)
        final_response.append (minimax (new_board, new_ttt_pos, oppid, myid, depth+1))
    return final_response

def get_possible_moves (board):
    return [key+1 for key in range(len(board)) if board[key]==0]

def make_move (board, possible_move, myid):
    board[possible_move - 1] = myid
    return board

def calculate_score (response, depth):
    scores = []
    temp_scores = []
    if (type(response[0]) != list):
        return [response[0], score_scaling (response[-2], response[-1])]
    for each_response in response:
        temp_scores.append (calculate_score (each_response, depth+1))

    if (depth != 1):
        scores = combine_scores(temp_scores)
    else:
        scores = temp_scores
    return scores

def combine_scores (temp_scores):
    count = 0
    total = 0
    for temp_score in temp_scores:
        count += 1
        total += temp_score[1]
    return [temp_scores[0][0], float(total)/float(count)]

def score_scaling (score, depth):
    if (score == game['myid']):
        return 10 - 2.5*(depth - 1)
    elif (score == game['oppid']):
        return -10 + 2.5*(depth - 1)
    else:
        return 0

def check_win (board):
    possible_wins = [
        [1,2,3],
        [4,5,6],
        [7,8,9], 
        [1,4,7],
        [2,5,8],
        [3,6,9],
        [1,5,9],
        [3,5,7]
    ]
    for i in range(2):
        player_pos = [key+1 for key in range(len(board)) if board[key] == game['ids'][i]]
        player_won = is_subset(possible_wins, player_pos)
        if player_won:
            return game['ids'][i]
    return 0

def is_subset (parent, child):
    for p_i in parent:
        present = True
        for p_i_j in p_i:
            if p_i_j not in child:
                present = False
                continue
        if present:
            return True
    return False

def init (board, myid, oppid, all_suggestion=False):
    game['myid'] = myid
    game['oppid'] = oppid
    game['ids'] = [game['myid'], game['oppid']]
    game['board'] = board

    merge_calculated_score = []
    # minimax algo
    response = minimax(game['board'], [], game['myid'], game['oppid'], 0)

    # scoring mechanism
    calculated_score = calculate_score (response, 1)
    merge_calculated_score.extend(calculated_score)
    new_calculated_score = sorted(merge_calculated_score, key=lambda x:x[1], reverse=True)

    if all_suggestion:
        return new_calculated_score

    return new_calculated_score[0][0]

def choose_final_response (calculated_score, macroboards_state):
    diff_threshold = 0.01
    response = 0

    for i in range(len(calculated_score)):
        if i == 0:
            response = calculated_score[i][0]
            response_score = calculated_score[i][1]
            response_fresh = macroboards_state['fresh'][response - 1]
            continue

        if response_fresh is not True and macroboards_state['fresh'][calculated_score[i][0] - 1] is True:
            if (calculated_score[0][1] - calculated_score[i][1] < diff_threshold):
                response = calculated_score[i][0]
                response_score = calculated_score[i][1]
                response_fresh = macroboards_state['fresh'][response - 1]

        if response_fresh is True:
            break
    return response
