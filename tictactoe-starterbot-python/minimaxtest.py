def init():
    import minimax as mm
    data = [
        [[0, 0, 2, 0, 0, 2, 0, 0, 1], [1], False],
        [[1, 2, 2, 0, 0, 0, 0, 0, 0], [9], False],
        [[0, 1, 0, 0, 2, 2, 0, 1, 0], [4], False],
        [[0, 1, 0, 0, 2, 2, 0, 1, 0], [4], False],
        [[0, 0, 0, 1, 0, 0, 2, 0, 0], [6], False],
        [[0, 0, 0, 0, 2, 0, 0, 1, 2], [1], False],
        [[0, 1, 0, 0, 2, 0, 0, 0, 0], [1, 3], True]
    ]
    i = 0
    for board in data:
        i += 1
        output = mm.init(board[0], 1, 2, board[2])
        if board[2] is True:
            print output
            continue
        if output in board[1]:
            print "success : "+str(i)+", actual o/p : " + str(output)
        else:
            print "failure : "+str(i)+", actual o/p : " + str(output)
