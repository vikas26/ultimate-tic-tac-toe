import math

class Position:
    
    def __init__(self):
        self.board = []
        self.macroboard = []
    
    def parse_field(self, fstr):
        flist = fstr.replace(';', ',').split(',')
        self.board = [ int(f) for f in flist ]
    
    def parse_macroboard(self, mbstr):
        mblist = mbstr.replace(';', ',').split(',')
        self.macroboard = [ int(f) for f in mblist ]
    
    def is_legal(self, x, y):
        mbx, mby = x/3, y/3
        return self.macroboard[3*mby+mbx] == -1 and self.board[9*y+x] == 0

    def legal_moves(self):
        return [ (x, y) for x in range(9) for y in range(9) if self.is_legal(x, y) ]
        
    def make_move(self, x, y, pid):
        mbx, mby = x/3, y/3
        self.macroboard[3*mby+mbx] = -1
        self.board[9*y+x] = pid
        
    def get_board(self):
        return ''.join(self.board, ',')

    def get_macroboard(self):
        return ''.join(self.macroboard, ',')

    def get_available_macroboards(self):
        return [x+1 for x in range(len(self.macroboard)) if self.macroboard[x] == -1]

    def minimax_legal_moves(self, macroboard_number):
        macroboard_number = [x+1 for x in range(len(self.macroboard)) if self.macroboard[x] == -1][0]
        minimax_board = self.get_board_data(macroboard_number)
        return (macroboard_number, minimax_board)

    def get_board_data(self, board_number):
        # 1-9
        board_data = []
        mx = ((board_number - 1) % 3) *3
        my = int(math.floor(((board_number - 1) / 3))) *3
        board_data.extend(self.board[9*(my+0)+mx:9*(my+0)+mx+3])
        board_data.extend(self.board[9*(my+1)+mx:9*(my+1)+mx+3])
        board_data.extend(self.board[9*(my+2)+mx:9*(my+2)+mx+3])
        return board_data