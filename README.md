# Ultimate Tic Tac Toe #

Ultimate Tic Tac Toe takes a simple game and turns it into something surprisingly fun. Ultimate Tic Tac Toe is vastly more complicated than tic tac toe. Each square of the Tic Tac Toe game contains another Tic Tac Toe game in it! Only if you win 3 smaller games in a row, you win the whole game.
Minimax algorithm is used to make a move in a smaller tic-tac-toe game.

### Live example of this code ###

It is live on [**The AI Games**](http://theaigames.com/). [**Here you can see one of the matches played by my AI.**](http://theaigames.com/competitions/ultimate-tic-tac-toe/games/57f16f5e4ee9c6649103b0aa)